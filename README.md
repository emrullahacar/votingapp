Example Voting App
=========

A simple distributed application running across multiple Docker containers.

Getting started
---------------

Run the app in Kubernetes
-------------------------

The folder helmfiles contains the values.yaml files of the Voting App's services.

Run the following command to create the deployments and services objects:
```
$ cd helmfiles
$ helm upgrade --install vote -f values/vote-values.yaml charts/app/. --set image.tag=${IMAGE_TAG}"
$ helm upgrade --install worker -f values/worker-values.yaml charts/app/. --set image.tag=${IMAGE_TAG}"
$ helm upgrade --install result -f values/result-values.yaml charts/app/. --set image.tag=${IMAGE_TAG}"
$ helm upgrade --install db-volume values/db-pv.yaml charts/host-persistence/."
$ helm upgrade --install db -f values/db-values.yaml charts/app/."
$ helm upgrade --install redis -f values/redis-values.yaml charts/app/."
```

The vote and result interface is then available on port 31001 and 31002 on each host of the cluster.

Architecture
-----

![Architecture diagram](architecture.png)

* A front-end web app in [Python](/vote) or [ASP.NET Core](/vote/dotnet) which lets you vote between two options
* A [Redis](https://hub.docker.com/_/redis/) or [NATS](https://hub.docker.com/_/nats/) queue which collects new votes
* A [.NET Core](/worker/src/Worker), [Java](/worker/src/main) or [.NET Core 2.1](/worker/dotnet) worker which consumes votes and stores them in…
* A [Postgres](https://hub.docker.com/_/postgres/) or [TiDB](https://hub.docker.com/r/dockersamples/tidb/tags/) database backed by a Docker volume
* A [Node.js](/result) or [ASP.NET Core SignalR](/result/dotnet) webapp which shows the results of the voting in real time


Notes
-----

The voting application only accepts one vote per client. It does not register votes if a vote has already been submitted from a client.

This isn't an example of a properly architected perfectly designed distributed app... it's just a simple
example of the various types of pieces and languages you might see (queues, persistent data, etc), and how to
deal with them in Docker at a basic level.
