### HELM Values

1. [**Overview**](#overview)
2. [**Chart structure**](#chart-structure)
3. [**Pipeline**](#deployment)

# Overview

This repository is created to deploy applications and other services to K8S with Helm. The repository contains values.yaml files of each resource in values folder.

# Chart structure

Repo has 3 type charts for applications, persistence volumes and Jenkins official chart.

Jenkins deployed by Jenkins official chart for Jenkins installation. Jenkins's persistence volume provided by host-persistince chart.

Vote, result and worker applications deployed by App chart. These applications have a values.yaml file in values folder with their names.

Postgresql(DB) deployed by app chart, also PostgreSQL's persistence volume provided by host-persistence chart.

```
charts:
    app:
        templates:
            service.yaml
            deployment.yaml
            serviceacccount.yaml
    host-persistence:
        templates:
            persistence-volume-claim.yaml
            persistence-volume.yaml
    jenkins:
        jenkins-official-chart-files
```

# Pipeline

There is a Jenkinsfile.deploy file in the repository to deploy these helm charts to the Kubernetes cluster via Jenkins.

Jenkins has a deploy job to pull this repository and run helm commands for deploying step.